FROM cypress/included:7.0.1

ADD . ./

RUN npm install
RUN npm run test